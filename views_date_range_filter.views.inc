<?php
/**
 * @file views_date_range_filter.views.inc
 */

/**
 * Implementation of hook_views_data().
 */
function views_date_range_filter_views_data() {
  $tables = module_invoke_all('date_api_tables');

  foreach ($tables as $base_table) {
    // The date range filter.
    $data[$base_table]['date_range_filter'] = array(
      'group' => t('Date'),
      'title' => t('Date range filter (!base_table)', array('!base_table' => $base_table)),
      'help' => t('Filter any Views !base_table date field with range options.', array('!base_table' => $base_table)),
      'filter' => array(
        'handler' => 'views_date_range_filter_handler_filter_date',
        'empty name field' => t('Undated'),
        'base' => $base_table,
      ),
    ); 
  }
  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function views_date_range_filter_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_date_range_filter'),
    ),
    'handlers' => array(
      'views_date_range_filter_handler_filter_date' => array(
        'parent' => 'date_api_filter_handler',
      ),
    ),
  );
}

