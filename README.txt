
This module provides an alternative date filter for Views which gives options like 'today', 'this week', etc.

Installation
============

To set up the filter:

- set granularity to 'day'
- set operator to 'between'

The available periods are hardcoded in the file views_date_range_filter_handler_filter_date.inc. You can hack them yourself, or provide a patch for an admin UI ;)

Status
======

This module was developed for a project's specific needs, and as such doesn't provide complete functionality. Users of this module are invited to take up the baton and provide patches for things such as an admin UI which is currently lacking.
