<?php

/**
 */
class views_date_range_filter_handler_filter_date extends date_api_filter_handler {
  // Override the setting of this to TRUE in views_handler_filter_numeric.
  // This means the 'Force single' option must be set manually in admin in most
  // use cases for this filter; however the presence of the option also means that
  // *no* option can be selected in the settings for the filter, and hence the
  // view with no applied filters shows everything rather than a default option
  // from this filter. Does that make sense? ;)
  var $no_single = FALSE;
  
  /**
   * Harcoded settings.
   *
   * TODO: provide an admin UI for choosing these.
   *
   * @param $op
   *  One of 'options' to return an options array for a form element, or 'min'
   *  or 'max' to return the date_modify() string for the respective part of
   *  the query.
   * @param $given_key
   *  With the 'min' or 'max' operation, the key to return the string for.
   */
  function date_range_ranges($op = 'options', $given_key = NULL) {
    static $ranges;
    if (!isset($ranges)) {
      // Ranges should be an array keyed by the arbitrary (but meaningful)
      // string which will appear in the URL.
      // The entries are arrays which should have the following keys:
      //  - label: the text for the filter dropdown
      //  - min: a string to pass to date_modify() to convert 'now' into the
      //    desired start date. 
      //  - max: likewise.
      //  If either min or max are omitted, then the date will not be altered.
      $ranges = array(
        '1D' => array(
          'label' => t('Today'),
          'max'   => '+ 0 day', // 'Today' is a special case: we don't actually want a 24-hour interval.
          // Though this might be a quirk of the granularity being set to 'day' on the current site :/
        ),
        '1W' => array(
          'label' => t('This week'),
          'max'   => '+ 1 week',
        ),
        '1M' => array(
          'label' => t('This month'),
          'max'   => '+ 1 month',
        ),
        '1M-1Y' => array(
          'label' => t('Next month and beyond'),
          'min'   => '+ 1 month',
          'max'   => '+ 1 year',
        ),
      );
    }
    
    switch ($op) {
      case 'options':
        // Return an array of options suitable for a form element.
        foreach ($ranges as $key => $data) {
          $options[$key] = $data['label'];
        }
        return $options;
      case 'max':
      case 'min':
        // Return a string suitable for date_modify() to use.
        // @TODO: go hunting in date_api() for something that does this!
        if (isset($ranges[$given_key][$op])) {
          return $ranges[$given_key][$op];
        }
        else {
          return '';
        }
    }
  }

  /**
   * Add the selectors to the value form using the date handler.
   *
   * @TODO: large chunks of this function could probably be removed;
   * it's currently merely aquickly hacked copy of the parent.
   */
  function value_form(&$form, &$form_state) {
    // We use different values than the parent form, so we must
    // construct our own form element.
    $form['value'] = array();
    $form['value']['#tree'] = TRUE;
    
    // We have to make some choices when creating this as an exposed
    // filter form. For example, if the operator is locked and thus
    // not rendered, we can't render dependencies; instead we only
    // render the form items we need.
    $which = 'all';
    $source = '';
    if (!empty($form['operator'])) {
      $source = ($form['operator']['#type'] == 'radios') ? 'radio:options[operator]' : 'edit-options-operator';
    }

    if (!empty($form_state['exposed'])) {
      if (empty($this->options['expose']['use_operator']) || empty($this->options['expose']['operator'])) {
        // exposed and locked.
        $which = in_array($this->operator, $this->operator_values(2)) ? 'minmax' : 'value';
      }
      else {
        $source = 'edit-' . form_clean_id($this->options['expose']['operator']);
      }
    }

    $handler = $this->date_handler;
       
    if ($which == 'all' || $which == 'value') {
      $form['value'] += $this->date_parts_form($form_state, 'value', $source, $which, $this->operator_values(1));
      if ($this->force_value) {
        $form['value']['value']['#force_value'] = TRUE;
      }
    }

    if ($which == 'all' || $which == 'minmax') {
      // @todo: add an extra option in the admin UI to switch on here!
      $form['value'] = array(
        '#type' => 'select',
        '#multiple' => TRUE,
        '#title' => t('Date'),
        '#default_value' => $this->value,
        '#options' => $this->date_range_ranges('options'),
      );
      
      /*
      $form['value'] += $this->date_parts_form($form_state, 'min', $source, $which, $this->operator_values(2));
      $form['value'] += $this->date_parts_form($form_state, 'max', $source, $which, $this->operator_values(2));
      if ($this->force_value) {
        $form['value']['min']['#force_value'] = TRUE;
        $form['value']['max']['#force_value'] = TRUE;
      }
      */
    }
    
    // Add some text to make it clear when additional options are available.
    $extra = '';
    if (version_compare(PHP_VERSION, '5.2', '>=')) {
      $extra = t(" You can use any values PHP's date_create() can understand, like between '12AM today' and '12AM tomorrow.");
    }
    $form['value']['value']['#prefix'] = '<div class="form-item"><label>' . t('Absolute value') . '</label></div>';
    $form['value']['default_date'] = array(
      '#type' => 'textfield',
      '#title' => t('Date default'),
      '#default_value' => $this->options['default_date'],
      '#prefix' => '<div class="form-item"><label>' . t('Relative value') . '</label><p>' . t("Relative values will be used if no date is set above. Use 'now' to default to the current date at runtime or add modifiers like 'now +1 day'. The To date default value is used when the operator is set to 'between' or 'not between'.") . $extra .'</p><p>' . t('If the filter is exposed, these values will be used to set the inital value of the exposed filter. Leave both date and default values blank to start with no value in the exposed filter.') .'</p></div>',
      );
    $form['value']['default_to_date'] = array(
      '#type' => 'textfield',
      '#title' => t('To date default'),
      '#default_value' => $this->options['default_to_date'],
      );

    // Test if this value is in the UI or exposed, only show these elements in the UI.
    // We'll save it as an option rather than a value to store it for use
    // in the exposed filter.
    if (!empty($form_state['exposed'])) {
      $form['value']['default_date']['#type'] = 'value';
      $form['value']['default_date']['#value'] = $form['value']['default_date']['#default_value'];
      $form['value']['default_to_date']['#type'] = 'value';
      $form['value']['default_to_date']['#value'] = $form['value']['default_to_date']['#default_value'];
      unset($form['value']['default_date']['#prefix']);
      unset($form['value']['value']['#prefix']);
    }
    
    $form['value']['#theme'] = 'date_views_filter_form';    
  }
  
  /**
   * Create date values for the parent::query() to find.
   */
  function query() {
    // Don't affect the query when the '<Any>' option is selected.
    if ($this->value[0]['value'] == 'All') {
      return;
    }

    // Convert the form select options key into strings for date_modify().
    if ($this->options['expose']['single']) {
      $offset_min = $this->date_range_ranges('min', $this->value[0]['value']);
      $offset_max = $this->date_range_ranges('max', $this->value[0]['value']);
    }
    else {
      // What to do when multiple options are selected is out of my scope right now, sorry...
      // @todo please file a patch! ;)
      //dsm($this->value);
      // This has keys => values as the initial options if there are some selected, and
      // value, min, max keys (though why!?!) if not.
    }
    $date_min = date_make_date(time());
    $date_max = date_make_date(time());

    // Damn PHP and its inconsistent functions.
    date_modify($date_min, $offset_min);
    date_modify($date_max, $offset_max);

    $value_min = date_format($date_min, DATE_FORMAT_DATETIME);
    $value_max = date_format($date_max, DATE_FORMAT_DATETIME);

    // Spoof a new value array.
    $this->value = array(
      'min' => $value_min,
      'max' => $value_max,
      'default_date' => '',
      'default_to_date' => '',
    );
    
    // Now call the parent query method, which will find the spoofed
    // value as if there had been a regular date filter form.
    parent::query();
  }  
}
